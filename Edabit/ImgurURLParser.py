import sys
import re #import regex module

def imgurUrlParser(url):
    url = url.split('/')
    length = len(url)
    id_type = {}
    if re.search('.com', url[length - 2]) != None:
        img = url[length - 1].split('.')
        img_id = img[0]
        img_type = 'image'
    elif url[length - 2] == 'gallery':
        img_id = url[length - 1]
        img_type = 'gallery'
    elif url[length - 2] == 'a':
        img_id = url[length - 1]
        img_type = 'album'
    else:
        print("Not valid imgur link")
        sys.exit(0)
    id_type = {'id': img_id, 'type': img_type}
    return id_type
if __name__ == "__main__":
    r = imgurUrlParser(sys.argv[1])
    print(r)
