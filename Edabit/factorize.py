import sys

def factorize(n):
	alist = []
	l = []
	factor = 2
	while n > 1:
		if n % factor == 0:
			alist.append(factor)
			n /= factor
		else:
			factor += 1
	for f in alist:
		count = alist.count(f)
		l.append((f, count))
	l = set(l)
	sort = sorted(l, key=lambda tup : tup[0])
	return sort

if __name__ == "__main__":
    n = int(sys.argv[1])
    print(factorize(n))
