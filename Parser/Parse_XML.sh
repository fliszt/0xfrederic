#!/bin/bash
#title                  :parseXML.sh
#description    :This script will look for XML files in subdirectories and parses the XML files for customer numbers.
#author                 :Laur Telliskivi
#date                   :20180524

#Store the parse path in variable
parse_path=$1
#Destination path for the output file
dest="<filepath>"
#Logging the script activity
log="<filepath>"

if [ -z "$parse_path" ]
then
        echo "Please set the full path as an argument where you want to parse .XML files."
        exit 1
else
        echo $(date) "Looking for .XML files in subdirectories..." >> $log
fi      
#Look for XML files in subdirectories and collect them to a variable
files=$(find $parse_path*/ -type f -name '*.XML')

#check if variable is null
if [ -z files ]
then
        echo $(date) "Could not find any .XML files" >> $log | mailx -r user@host -s "No .XML files in <filepath> subdirectories." email@email
        echo "------" >> $log 
        exit 1
else
        echo $(date) ".XML files found. Parsing now..." >> $log
fi

#go through every XML file in files list
for xml in $files
do      
        #store the folder name where the XML file is in
        date=$(dirname $xml | awk -F"/" '{print $NF}')
        #parse customer numbers from XML file and print 'customer,folder name'
        grep 'cust_number' $xml | awk -F">" '{print $2}' | awk -v d=${date} -v m=$(date +%m) -F"<" '{print $1 "," d m}'
done > $dest/invoice_dates.txt #write result to a file

#check if file size is greater than zero
if [ -s $dest/invoice_dates.txt ]
then
        echo $(date) "Parsing successful! Copying output file to ..." >> $log
        scp $dest/invoice_dates.txt user@host:/filepath
        if [ $? -eq 0 ]
        then
                echo $(date) "Copying successful!" >> $log #| mailx -r tms@sunrise.int.radiolinja.ee -s "Invoice dates exported to elisa-ai." TMS@elisa.ee, app.admins@elisa.ee
        else
                echo $(date) "Unable to connect to elisa-ai..." >> $log | mailx -r user@host -s "Unable to connect to elisa-ai server!" email@email
        fi      
else
        echo $(date) "Output file empty. Do nothing" >> $log
fi      
echo "------" >> $log