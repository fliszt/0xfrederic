#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the hourglassSum function below.
def hourglassSum(arr):
    l = []
    ver = 0
    hor = 0
    s = 0
    while ver < 4:
        while hor < 4:
            for y in range(ver,ver+3):
                if y == ver+1:
                    s += arr[y][hor+1]
                else:
                    for x in range(hor,hor+3):
                        s += arr[y][x]
            l.append(s)
            s = 0
            hor += 1
        hor = 0
        ver += 1
    m = max(l)
    return m
    
            
   

if __name__ == '__main__':
    arr = [[1, 1, 1, 0, 0, 0],
            [0, 1, 0, 0, 0, 0],
            [1, 1, 1, 0, 0, 0],
            [0, 0, 2, 4, 4, 0],
            [0, 0, 0, 2, 0, 0],
            [0, 0, 1, 2, 4, 0]]
    result = hourglassSum(arr)

    print(result)

