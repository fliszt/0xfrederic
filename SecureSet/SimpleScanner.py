# Title:  Simple port scanner
# Author: Laur Telliskivi
# Date:   25 Sept 2018

#!/usr/bin/env python3
'''
This program allows user to sockets providing IP address and Port number
User must provide either IP address or IP address with CIDR (e.g. /24)
Port number can be specified as a single port, multiple ports (separated with commas) or a range (e.g. 1-100)
If no port number is provided, default well known ports range is used.
'''
import sys
import re #re module provides regular expression matching operations
import socket #socket module provides access to the BSD socket interface.
import ipaddress #ipaddress module provides the capabilities to create, manipulate and operate on IPv4 and IPv6 addresses and networks.

WKPORTS = '1-1024'#constant for well known ports which we shall use if user does not specify ports

#This function is used to parse the IP address argument
def parse_address(ip):
    if re.fullmatch("\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/[1-3][0-9]?", ip): #Checking only structure of IP address input
        addr, nw = ip.split('/') #Extract ip and CIDR
        return ip, nw
    elif re.fullmatch("\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}", ip): #Checking if only IP w/o CIDR was given as argument
        return ip, False 
    else:
        print("Invalid IP address format.")
        sys.exit(0)
        
#This function is used to parse the port(s) argument
def parse_port(port):
    ports = [] #Create a empty list to store user specified port(s)
    if re.fullmatch('\d{1,5}-\d{1,5}', port) != None: #Checking if port range was given
        port_range = port.split('-')
        fport = int(port_range[0]) #First port of the range
        lport = int(port_range[1]) #Last port of the range
        for p in range(fport,lport + 1):
            ports.append(p) #store every single port in the range to a list
        return ports
    elif re.fullmatch('\d{1,5}', port) != None: #Checking if single port was given
        ports.append(port)
        return ports
    elif re.fullmatch('(?:\d{1,5},)+(?:\d{1,5})', port) != None: #Checking if multiple ports were given but not a range!
        ports = port.split(',')
        return ports
    else:
        print(port + ' this does not seem to be correct port.')
        sys.exit(0)

#This function performs the actual port scan which takes IP address and port number as arguments
def scan(ip, port):
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s: #open a socket
            s.settimeout(1) #wait 1s before attempting to connect to a port
            result = s.connect_ex((ip, port)) #connect
        if result == 0: #Success
            print('Connection to ' + ip + ' port ' + str(port) + ' succeeded!')
        else:
            print(ip + ' Port ' + str(port) + ' closed.')
    except socket.gaierror: #Catch invalid IP addresses
        print('IP address octet value must be between 0 and 255 including')
        sys.exit(0)

if __name__ == "__main__":
    if len(sys.argv) == 2:
        sys.argv.append(WKPORTS)
    if len(sys.argv) == 3:
        ip = parse_address(sys.argv[1])
        port = parse_port(sys.argv[2])
        if ip[1] != False: #If CIDR was specified as part of IP address
            ips = ipaddress.ip_network(ip[0]) #Get all ip addresses in the specified network
            for i in ips:
                for p in port:
                    try:
                        scan(str(i),int(p))
                    except KeyboardInterrupt: #Just in case I want to cancel the scan
                        sys.exit(0)
        else:
            for p in port: #If CIDR was not specified
                try:
                    scan(ip[0],int(p))
                except KeyboardInterrupt:
                    sys.exit(0)
    else:
        print('Usage: <IP> <Port(s)>')
        print('Examples: ')
        print(sys.argv[0] + ' 192.168.0.0/24 1-100')
        print(sys.argv[0] + ' 192.168.0.0 1,53,22')
        print(sys.argv[0] + ' 192.168.0.5')
