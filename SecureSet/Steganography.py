# Title:  String hidden within another string
# Author: Laur Telliskivi
# Date:   20 Sept 2018

import sys
# input: 'yohelloyo' 'hello' ; output: 'yes' 
# input: 'yHsEsLcLcOsaer' 'hello' ; output: 'yes'
# input: 'hseplpljko' 'hello' ; output: 'no'

if len(sys.argv) == 3:
    longW = sys.argv[1]
    shortW = sys.argv[2]
    times = len(longW) // len(shortW)
    flag = False
    level = 1
    shift = 0
    wordMask = ''
    i = 0
    gotIt = False
    while level < times + times + 1: 
        while i < len(shortW):
            if shift + i * level < len(longW):
                wordMask += longW[shift + i * level]
            if i < len(shortW):
                if shift + i * level == len(longW) - 1 :
                    flag = True
            i += 1
        print(wordMask)
        if wordMask == shortW:
            print("yes")
            sys.exit(0)
        if flag:
            level += 1
            shift = 0 
            i = 0 
            wordMask = ''
            flag = False 
        else:
            shift += 1
            i = 0
            wordMask = ''
        
print("no")
