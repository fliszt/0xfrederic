# Title:  Is there a repetition in the word?
# Author: Laur Telliskivi
# Date:   2 Sept 2018

import sys

if len(sys.argv) == 2:
    arg1 = sys.argv[1]
    length = len(arg1)
    i = 0
    n = 0
    count = 0
    while i < length:
        if n < length:
            if arg1[i + n] == arg1[i]:
                count += 1
                n += 1
            else:
                n += 1
        if i + n == length:
            i += 1
            n = 0

    if count > length:
        print('has repetition')
    else:
        print('no repetition')
