# Title:  Is it anagram?
# Author: Laur Telliskivi
# Date:   4 October 2018

import sys
#read in arguments
arg1 = sys.argv[1]
arg2 = sys.argv[2]
i = 0
lx = [] #list for first arg
ly = [] #list for second arg

# do the magic only when both args are the same length
if len(arg1) == len(arg2):

    #write the args to list letter by letter
    while i < len(arg2):
        lx.append(arg2[i])
        ly.append(arg1[i])
        i += 1
    i = 0
    j = 0
    ln = len(ly)

    #Compare every letter in first arg to the letters in second arg
    #If there is a match, remove the letter from both arg
    while i < len(lx):
        while j < len(ly):
            if lx[i] == ly[j]:
                lx.pop(i)
                ly.pop(j)
                j = 0
            else:
                j += 1
        i += 1
        j = 0

    #we have an anagram if the both of the lists are empty after comparing all letters    
    if len(lx) == 0:
        print('yay')
    else:
        print('nay')
else:
    print('nay')
