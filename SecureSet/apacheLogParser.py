#!/use/bin/env python3

# Script prints out lines that have private IP address.

import sys
fpath = sys.argv[1]

l = ''
for i in range(16, 32):
    l += str(i) + ','

f = open(fpath, 'r')
line = f.readline()
while line != '':
    ip = line.split(' ')[0].split('.')
    if ip[0] == '10':
        print(line)
    elif ip[0] == '192' and ip[1] == '168':
        print(line)
    elif ip[0] == '172' and l.find(ip[1]) != -1:
        print(line)
    line = f.readline()
f.close()
