#!/usr/bin/env python3
#title                  :count_letters
#description            :This script will count each alhpabet letter count in given file and calculates their frequencies
#author                 :Laur Telliskivi
#date                   :20180817

import sys

#Set letter count and frequency variables to 0
total_count = 0
letter_freq = 0

#Create a dictionary to hold letter counts
my_dict = {}

#Read file from filepath and store it to a variable
with open(sys.argv[1],'r') as text:
    ciphertext = text.read()

for letter in ciphertext:
    if letter == ' ':
        continue
    elif letter in my_dict:
        my_dict[letter] += 1
    else:
        my_dict[letter] = 1
#Sort letter counts in descending order
sorted_my_dict = sorted(my_dict.items(), key=lambda kv: kv[1], reverse=True)
print(sorted_my_dict)

for key in my_dict:
    total_count += my_dict[key]
#Print letter frequencies
for key in my_dict:
    letter_freq = round(my_dict[key] / total_count * 100, 2)
    print (key, letter_freq)

