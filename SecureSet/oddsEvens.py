import sys

l = sys.argv[1:]
odds = True
even = True

if len(l[0::2]) > 0:
    for num in l[0::2]:
        if int(num) % 2 == 0:
            odds = False
    if len(l[1::2]) > 0:
        for num in l[1::2]:
            if int(num) % 2 != 0:
                even = False
if odds == True and even == True:
    print("yes")
else:
    print("no")


