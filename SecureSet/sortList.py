# Title:  List sorting ASC
# Author: Laur Telliskivi
# Date:   12 October 2018
import sys

l = sys.argv[1:] #create a list of arguments
largest = int(l[0]) #initialize the largest value to first num in list
largestI = 0 #for keeping track of the largest num index
i = 0 
while i < len(l):

    # Compare largest num to every num in list, if there is any num greater
    # swap places in the list
    for num in range(i, len(l)):
        if int(largest) < int(l[num]):
            largest = int(l[num])
            largestI = num 
    x = l[i] 
    l[i] = largest
    l[largestI] = int(x)
    i = i + 1
    if i < len(l):
        largest = l[i] #set the next largest num
        largestI = i #reset the next large num index

print(l)

