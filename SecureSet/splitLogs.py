import sys

path = sys.argv[1]
file_count = 1
line_count = 0
with open(path, "r") as inputfile:
    l = inputfile.readline()
    while l != '':
        dest = "system" + "." + str(file_count)
        while line_count < 100:
            with open(dest,"a") as output:
                output.write(l)
                l = inputfile.readline()
                line_count += 1
        line_count = 0
        file_count += 1

