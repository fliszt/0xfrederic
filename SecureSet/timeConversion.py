#!/bin/python3
#title                  :Time Conversion
#description            :Program will convert 12 hour time to 24 hour time
#author                 :Laur Telliskivi
import os
import sys

#
# Complete the timeConversion function below.
#
def timeConversion(s):
    missingHours = [00,13,14,15,16,17,18,19,20,21,22,23,12]
    time = s.split(":")
    if time[2][-2:] == "PM":
        time[0] = missingHours[int(time[0])]
        time[2] = time[2][:2]
    else:
        if time[0] == "12":
            time[0] = "00"
        time[2] = time[2][:2]

    time = [str(x) for x in time]
    time = ":".join(time)
    return time

if __name__ == '__main__':
    
    print("Enter 12 hour time: ")
    s = input()

    result = timeConversion(s)
    
    print("24 hour time: " + result)
