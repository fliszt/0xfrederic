# Title:  Web Spider
# Author: Laur Telliskivi
# Date:   9 November 2018
import requests
import re
from bs4 import BeautifulSoup
import sys

URL = sys.argv[1]
linksVisited = []
linksFound = [URL]
newLinks = True
i = 0

# open file for writing
f = open("temp.dat","w")
while newLinks:

    try:
        # get page
        page = requests.get(URL)
    except KeyboardInterrupt:
        sys.exit(0)
    except: 
        pass # if conn failes, keep on going
    
    # get contents of the page
    contents = page.content

    # Create BeautifulSoup object and use python's 
    # built in html parser. Beautiful Soup is a Python 
    # library for pulling data out of HTML and XML files.
    soupObj = BeautifulSoup(page.content, 'html.parser')
    for link in soupObj.find_all('a'): # find all 'a' tags
        l = str(link.get('href')) # get the URL's from the tag
        if re.match('http',l) != None and l not in linksFound:
            linksFound.append(link.get('href'))
            print("Scraping [+] : " + l)
            f.write(l + "\n")
    if i + 1 < len(linksFound):
        i = i + 1
    if linksFound[i] not in linksVisited:
        linksVisited.append(URL)
        URL = linksFound[i]
    else:
        newLinks = False
f.close()
